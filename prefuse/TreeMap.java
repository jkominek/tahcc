import java.util.*;
import java.io.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import prefuse.Display;
import prefuse.Visualization;
import prefuse.action.ActionList;
import prefuse.action.RepaintAction;
import prefuse.action.animate.ColorAnimator;
import prefuse.action.assignment.*;
import prefuse.action.layout.graph.SquarifiedTreeMapLayout;
import prefuse.controls.ControlAdapter;
import prefuse.data.*;
import prefuse.data.io.*;
import prefuse.data.query.SearchQueryBinding;
import prefuse.render.DefaultRendererFactory;
import prefuse.render.ShapeRenderer;
import prefuse.util.ColorLib;
import prefuse.util.ColorMap;
import prefuse.util.FontLib;
import prefuse.util.UpdateListener;
import prefuse.util.ui.JFastLabel;
import prefuse.util.ui.JSearchPanel;
import prefuse.util.ui.UILib;
import prefuse.visual.NodeItem;
import prefuse.visual.VisualItem;
import prefuse.visual.VisualTree;
import prefuse.visual.sort.TreeDepthItemSorter;

import org.dom4j.*;
import org.dom4j.io.*;
import com.csvreader.*;

/**
 * Demonstration showcasing a TreeMap layout of a hierarchical data
 * set and the use of dynamic query binding for text search. Animation
 * is used to highlight changing search results.
 *
 * @author <a href="http://jheer.org">jeffrey heer</a>
 */
public class TreeMap extends Display {

    private SearchQueryBinding searchQ;

    private static final String tree = "tree";
    private static final String treeNodes = "tree.nodes";
    private static final String treeEdges = "tree.edges";
    
    public TreeMap(Tree t, String[] labels, String colorlabel) {
        super(new Visualization());
        
        VisualTree vt = m_vis.addTree(tree, t);
        m_vis.setVisible(treeEdges, null, false);
            
        m_vis.setRendererFactory(
            new DefaultRendererFactory(new TreeMapRenderer(labels)));

        Set<String> all_color_values = new TreeSet<String>();
        for(int i=0; i<t.getNodeTable().getRowCount(); i++)
            {
                String v = t.getNodeTable().getTuple(i).getString(colorlabel);
                if( (v!=null) && (v.length()>0) )
                    all_color_values.add( v );
            }                       
        // border colors
        final ColorAction borderColor = new BorderColorAction(treeNodes);
        final ColorAction fillColor = new FillColorAction(treeNodes, colorlabel, all_color_values);
        final FontAction fontAction = new MyFontAction(treeNodes, "Default", 24);

        
        // full paint
        ActionList fullPaint = new ActionList();
        fullPaint.add(fillColor);
        fullPaint.add(borderColor);
        fullPaint.add(fontAction);
        m_vis.putAction("fullPaint", fullPaint);
        
        // animate paint change
        ActionList animatePaint = new ActionList(400);
        animatePaint.add(new ColorAnimator());
        animatePaint.add(new RepaintAction());
        m_vis.putAction("animatePaint", animatePaint);
        
        // create the single filtering and layout action list
        ActionList layout = new ActionList();
        layout.add(new SquarifiedTreeMapLayout(tree));
        layout.add(fillColor);
        layout.add(borderColor);
        layout.add(fontAction);
        layout.add(new RepaintAction());
        m_vis.putAction("layout", layout);
        
        // initialize our display
        setSize(700,600);
        setItemSorter(new TreeDepthItemSorter());
        addControlListener(new ControlAdapter() {
            public void itemEntered(VisualItem item, MouseEvent e) {
                item.setStrokeColor(borderColor.getColor(item));
                item.getVisualization().repaint();
            }
            public void itemExited(VisualItem item, MouseEvent e) {
                item.setStrokeColor(item.getEndStrokeColor());
                item.getVisualization().repaint();
            }           
        });
        
        searchQ = new SearchQueryBinding(vt.getNodeTable(), labels[0]);
        m_vis.addFocusGroup(Visualization.SEARCH_ITEMS, searchQ.getSearchSet());
        searchQ.getPredicate().addExpressionListener(new UpdateListener() {
            public void update(Object src) {
                m_vis.cancel("animatePaint");
                m_vis.run("fullPaint");
                m_vis.run("animatePaint");
            }
        });

        // perform layout
        m_vis.run("layout");
    }
    
    public SearchQueryBinding getSearchQuery() {
        return searchQ;
    }
    
    public static void main(String argv[])
        throws Exception
    {
        UILib.setPlatformLookAndFeel();
        
        JComponent treemap = demo(argv[0], Arrays.copyOfRange(argv, 1, argv.length-1), argv[argv.length-1]);
        
        JFrame frame = new JFrame("p r e f u s e  |  t r e e m a p");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(treemap);
        frame.pack();
        frame.setVisible(true);
    }

    private static String branchLeafAttributeValue(Element bl, String attr)
    {
        Iterator eit = bl.elementIterator();
        while(eit.hasNext())
            {
                Element child = (Element)eit.next();
                if(child.getName().equals("attribute") &&
                   child.attributeValue("name").equals(attr))
                    return child.attributeValue("value");
            }
        return null;
    }

    public static Tree readCSVToTree(String filename, final String[] labelOrder)
        throws Exception
    {
        DocumentFactory df = new DocumentFactory();
        Document tree_doc = df.createDocument(df.createElement("tree"));
        Element tree = tree_doc.getRootElement();
        Element declarations = tree.addElement("declarations");
        Element root_branch = tree.addElement("branch");

        CsvReader reader = new CsvReader(filename);
        reader.readHeaders();
        String[] headers = reader.getHeaders();

        int[] mapping = new int[labelOrder.length];

        for(int i=0; i<headers.length; i++)
            {
                Element decl = declarations.addElement("attributeDecl");
                decl.addAttribute("name", headers[i]);
                decl.addAttribute("type", "String");
                for(int j=0; j<labelOrder.length; j++)
                    if(labelOrder[j].equals(headers[i]))
                        mapping[j] = i;
            }

        int count = 0;
        while(reader.readRecord())
            {
                String[] values = reader.getValues();
                Element parent = root_branch;
                for(int i=0; i<mapping.length; i++)
                    {
                        
                        boolean found = false;
                        Iterator eit = parent.elementIterator();
                        while(eit.hasNext())
                            {
                                Element child = (Element)eit.next();
                                String v = branchLeafAttributeValue(child, headers[mapping[i]]);
                                if( (v!=null) && v.equals(values[mapping[i]]) )
                                    {
                                        parent = child;
                                        found = true;
                                        break;
                                    }
                            }
                        if(!found)
                            {
                                Element next_parent = parent.addElement("branch");
                                for(int j=0; j<mapping.length; j++)
                                    {
                                        Element attr = next_parent.addElement("attribute");
                                        attr.addAttribute("name", headers[mapping[j]]);
                                        if(j<=i)
                                            attr.addAttribute("value", values[mapping[j]]);
                                        else
                                            attr.addAttribute("value", "");
                                    }
                                parent = next_parent;
                            }
                    }
                Element leaf = parent.addElement("leaf");
                for(int j=0; j<values.length; j++)
                    {
                        Element attr = leaf.addElement("attribute");
                        attr.addAttribute("name", headers[j]);
                        attr.addAttribute("value", values[j]);
                    }
            }

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        (new XMLWriter(output)).write(tree_doc);

        InputStream is = new ByteArrayInputStream(((ByteArrayOutputStream) output).toByteArray());

        return (Tree)((new TreeMLReader()).readGraph(is));
    }

    public static JComponent demo(String datafile, final String[] labelOrder, final String colorlabel)
        throws Exception
    {
        Tree t = readCSVToTree(datafile, labelOrder);

        // create a new treemap
        final TreeMap treemap = new TreeMap(t, labelOrder, colorlabel);
        
        // create a search panel for the tree map
        JSearchPanel search = treemap.getSearchQuery().createSearchPanel();
        search.setShowResultCount(true);
        search.setBorder(BorderFactory.createEmptyBorder(5,5,4,0));
        search.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 11));
        
        final JFastLabel title = new JFastLabel("                 ");
        title.setPreferredSize(new Dimension(350, 20));
        title.setVerticalAlignment(SwingConstants.BOTTOM);
        title.setBorder(BorderFactory.createEmptyBorder(3,0,0,0));
        title.setFont(FontLib.getFont("Tahoma", Font.PLAIN, 16));
        
        treemap.addControlListener(new ControlAdapter() {
            public void itemEntered(VisualItem item, MouseEvent e) {
                title.setText(item.getString(labelOrder[0]));
            }
            public void itemExited(VisualItem item, MouseEvent e) {
                title.setText(null);
            }
        });
        
        Box box = UILib.getBox(new Component[]{title,search}, true, 10, 3, 0);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(treemap, BorderLayout.CENTER);
        panel.add(box, BorderLayout.SOUTH);
        UILib.setColor(panel, Color.BLACK, Color.GRAY);
        return panel;
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * Set the stroke color for drawing treemap node outlines. A graded
     * grayscale ramp is used, with higer nodes in the tree drawn in
     * lighter shades of gray.
     */
    public static class BorderColorAction extends ColorAction {
        
        public BorderColorAction(String group) {
            super(group, VisualItem.STROKECOLOR);
        }
        
        public int getColor(VisualItem item) {
            NodeItem nitem = (NodeItem)item;
            if ( nitem.isHover() )
                return ColorLib.rgb(99,130,191);
            
            int depth = nitem.getDepth();
            if ( depth < 2 ) {
                return ColorLib.gray(100);
            } else if ( depth < 4 ) {
                return ColorLib.gray(75);
            } else {
                return ColorLib.gray(50);
            }
        }
    }
    
    /**
     * Set fill colors for treemap nodes. Search items are colored
     * in pink, while normal nodes are shaded according to their
     * depth in the tree.
     */
    public static class FillColorAction extends ColorAction {
        private int[] palette;
        private HashMap<String, Integer> mapping;
        private String m_label;

        public FillColorAction(String group, String label, Set<String> values) {
            super(group, VisualItem.FILLCOLOR);
            // we need to set it up so every value maps to another color
            m_label = label;
            palette = ColorLib.getHotPalette(values.size());
            mapping = new HashMap<String, Integer>(values.size()*2);
            Iterator<String> i = values.iterator();
            int idx=0;
            while(i.hasNext())
                {
                    mapping.put(i.next(), palette[idx++]);
                }
        }
        
        public int getColor(VisualItem item) {
            if ( m_vis.isInGroup(item, Visualization.SEARCH_ITEMS) )
                return ColorLib.rgb(32,32,240);
            
            if(item instanceof NodeItem)
                {
                    String v = item.getString(m_label);
                    if(mapping.containsKey(v))
                        return mapping.get(v);//.intValue();
                }
            return palette[0];
        }
        
    } // end of inner class TreeMapColorAction

    public class MyFontAction extends FontAction {
        Font[] fontbydepth;
        final int min_size = 5;
        public MyFontAction(String group, String fontname, int size)
        {
            super(group);
            fontbydepth = new Font[10];
            int cur_size = size;
            for(int i=0; i<fontbydepth.length; i++)
                {
                    fontbydepth[i] = new Font(fontname, Font.PLAIN, cur_size);
                    cur_size = (int)(cur_size * 0.6);
                    if(cur_size < min_size)
                        cur_size = min_size;
                }
        }

        public Font getFont(VisualItem item)
        {
            int d = ((NodeItem)item).getDepth();
            if(d>0) d -= 1;
            if(d < fontbydepth.length)
                return fontbydepth[d];
            else
                return fontbydepth[fontbydepth.length-1];
        }

        public Font getDefaultFont() { return fontbydepth[0]; }
    }

    /**
     * A renderer for treemap nodes. Draws simple rectangles, but defers
     * the bounds management to the layout. Leaf nodes are drawn fully,
     * higher level nodes only have their outlines drawn. Labels are
     * rendered for top-level (i.e., depth 1) subtrees.
     */
    public static class TreeMapRenderer extends ShapeRenderer {
        private Rectangle2D m_bounds = new Rectangle2D.Double();
        private String[] m_labels;
        
        public TreeMapRenderer(String[] labels) {
            m_manageBounds = false;
            m_labels = labels;
        }
        public int getRenderType(VisualItem item) {
            if ( ((NodeItem)item).getChildCount() == 0 ) {
                // if a leaf node, both draw and fill the node
                return RENDER_TYPE_DRAW_AND_FILL;
            } else {
                // if not a leaf, only draw the node outline
                return RENDER_TYPE_DRAW;
            }
        }
        protected Shape getRawShape(VisualItem item) {
            m_bounds.setRect(item.getBounds());
            return m_bounds;
        }
        
        public void render(Graphics2D g, VisualItem item) {
            super.render(g, item);
            int d = ((NodeItem)item).getDepth();
            // if a top-level node, draw the category name
            if ( (0<d) && (d<=2) ) {
                Rectangle2D b = item.getBounds();

                String s = item.getString(m_labels[((NodeItem)item).getDepth()-1]);
                //if( (s==null) || (s.length()==0) )
                //    return;
                Font f = item.getFont();
                FontMetrics fm = g.getFontMetrics(f);
                int w = fm.stringWidth(s);
                int h = fm.getAscent();
                g.setFont(f);
                g.setColor(Color.LIGHT_GRAY);
                g.drawString(s, (float)(b.getCenterX()-w/2.0),
                                (float)(b.getCenterY()+h/2));
            }
        }
        
    } // end of inner class NodeRenderer
    
} // end of class TreeMap
