import org.json.simple.*;
import org.sikuli.api.*;
import org.sikuli.api.robot.*;

import java.net.*;
import java.io.*;
import java.util.*;

import java.awt.event.KeyEvent;
import javax.imageio.ImageIO;

public class Demoizer
{
    private List<URL> thumbnail_urls;

    private Demoizer()
        throws Exception
    {
        findThumbnails(new URL("http://kominekwg.appspot.com/piclist.json"));
    }

    private void findThumbnails(URL jsonUrl)
        throws Exception
    {
        JSONArray piclist = (JSONArray)(JSONValue.parse(new InputStreamReader(jsonUrl.openStream())));
        
        thumbnail_urls = new ArrayList<URL>(50);

        for(int i=0; i<piclist.size(); i++)
            {
                JSONArray pair = (JSONArray)piclist.get(i);
                URL url = new URL((String)pair.get(0));
                thumbnail_urls.add(url);
            }
    }

    static Mouse mouse = new Mouse();
    static Keyboard keyboard = new Keyboard();

    private void demo()
        throws Exception
    {
        ScreenRegion s = new ScreenRegion();

        Target emptyTarget = new ColorImageTarget(ImageIO.read(new File("background-edge.png")));
        //emptyTarget.setMinScore(0.8);

        Target[] imageTargets = new Target[thumbnail_urls.size()];

        for(int i=0; i<thumbnail_urls.size(); i++)
            {
                imageTargets[i] = new ImageTarget(thumbnail_urls.get(i));
                imageTargets[i].setMinScore(0.6);
            }

        for(int i=0; i<imageTargets.length; i++)
            {
                System.out.println("looking for "+thumbnail_urls.get(i));
                int tries = 0;
                ScreenRegion thumb = s.find(imageTargets[i]);
                

                while(thumb==null && tries<4)
                    {
                        keyboard.keyDown(KeyEvent.VK_DOWN);
                        keyboard.keyUp(KeyEvent.VK_DOWN);
                        thumb = s.find(imageTargets[i]);
                        try {
                            Thread.sleep(100);
                        } catch(Exception e) {
                            // do not care even a little bit
                        }
                        tries++;
                    }

                if(thumb!=null)
                    mouse.click(thumb.getCenter());

                try {
                    Thread.sleep(400);//1500);
                } catch(Exception e) {
                    // do not care even a little bit
                }

                ScreenRegion nothing = s.find(emptyTarget);
                if(nothing!=null)
                    mouse.click(nothing.getCenter());

                if(false)
                for(int j=(i+1)%imageTargets.length; j!=i; j = (j+1)%imageTargets.length)
                    {
                        System.out.println("   trying to off click on "+thumbnail_urls.get(j));
                        ScreenRegion outside_image = s.find(imageTargets[j]);

                        if(outside_image!=null)
                            {
                                mouse.click(outside_image.getCenter());
                                break;
                            }
                    }
            }
        
    }

    public static void main(String[] args)
        throws Exception
    {
        (new Demoizer()).demo();
    }
}
