import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import javax.imageio.*;

class SepiaFilter
    extends RGBImageFilter
{
    public SepiaFilter()
    {
        // we don't depend on x & y, so we can filter color tables
        canFilterIndexColorModel = true;
    }

    public int filterRGB(int x, int y, int rgb)
    {
        // don't use x,y. see above
        double r = ((double)((rgb & 0x00ff0000)>>16));
        double g = ((double)((rgb & 0x0000ff00)>> 8));
        double b = ((double)((rgb & 0x000000ff)>> 0));

        // http://vizsketch.blogspot.com/2012/04/how-not-to-make-sepia-filter.html
        double intensity = 26.718;
        double greyscale = (r * 0.299) + (g * 0.587) + (b * 0.114);
        r = (greyscale + 0.956 * intensity);
        g = (greyscale - 0.272 * intensity) * 1.06;
        b = (greyscale - 1.105 * intensity) * 0.95;

        // pass through the alpha value
        rgb = rgb & 0xff000000;

        if( r<0.0 )
            /* do nothing */;
        else if( r>255.0 )
            rgb |= 0x00ff0000;
        else
            rgb |= ((int)(r))<<16;

        if( g<0.0 )
            /* do nothing */;
        else if( g>255.0 )
            rgb |= 0x0000ff00;
        else
            rgb |= ((int)(g))<< 8;

        if( b<0.0 )
            /* do nothing */;
        else if( b>255.0 )
            rgb |= 0x000000ff;
        else
            rgb |= ((int)(b))<< 0;

        return rgb;
    }
}

class PicturePage
	extends JPanel
	implements MouseListener, MouseMotionListener, MouseWheelListener, KeyListener
{
	// note: not actually a Picture Page. sorry bill
	private Image source_image;
	private Image current_image;
	private double scaling;
	
    private boolean scaling_up_to_date = true;
	private int fixed_offset_x = 0, fixed_offset_y = 0;
	private int dynamic_offset_x = 0, dynamic_offset_y = 0;
	private int drag_start_x = 0, drag_start_y = 0;
	
	public PicturePage()
	{
		super();
		this.addMouseWheelListener(this);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	
	void setImage(BufferedImage i)
	{
		source_image = i;
		current_image = i;
		scaling = 1.0;
		repaint(); // important!
	}

    void applyFilter(ImageFilter f)
    {
        ImageProducer producer = new FilteredImageSource(source_image.getSource(), f);
        source_image = createImage(producer);

        scaling_up_to_date = false;
        repaint();
    }
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

        if(current_image == null)
            return;

        if(!scaling_up_to_date)
        {
            current_image = source_image.getScaledInstance((int)(source_image.getWidth(null) * scaling), -1, Image.SCALE_SMOOTH);
            scaling_up_to_date = true;
        }

        g.drawImage(current_image, dynamic_offset_x, dynamic_offset_y, null);
	}
	
    public void mousePressed(MouseEvent e) {
    	drag_start_x = e.getXOnScreen();
    	drag_start_y = e.getYOnScreen();
    }

    public void mouseReleased(MouseEvent e) {
		fixed_offset_x = dynamic_offset_x;
		fixed_offset_y = dynamic_offset_y;
    }
     
	public void mouseDragged(MouseEvent e) {
		dynamic_offset_x = fixed_offset_x + (e.getXOnScreen() - drag_start_x);
		dynamic_offset_y = fixed_offset_y + (e.getYOnScreen() - drag_start_y);
		repaint();
	}

	public void mouseWheelMoved(MouseWheelEvent e)
	{
		int notches = e.getWheelRotation();
		if(notches < 0)
			scaling *= 0.95;
		else if(notches > 0)
			scaling *= 1.05;
		else
			return;

        scaling_up_to_date = false;
		repaint();
	}
	
	public void keyPressed(KeyEvent e)
	{	
		String x;
		if(e.getKeyCode()==39)
			fixed_offset_x = dynamic_offset_x = (fixed_offset_x + 10);
		else if(e.getKeyCode()==37)
			fixed_offset_x = dynamic_offset_x = (fixed_offset_x - 10);
		else if(e.getKeyCode()==38)
			fixed_offset_y = dynamic_offset_y = (fixed_offset_y - 10);
		else if(e.getKeyCode()==40)
			fixed_offset_y = dynamic_offset_y = (fixed_offset_y + 10);
		else
			return;
		repaint();
	}
	
	public void keyReleased(KeyEvent e)
	{	
		
	}

	public void keyTyped(KeyEvent e)
	{	
		
	}

	public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
    }
	
	public void mouseMoved(MouseEvent e) {
	}
}

public class PhotoViewer
	extends JFrame
	implements ActionListener, ItemListener
{

	private PicturePage pp;
	private BufferedImage current_image;
	
	JMenuItem loadFileItem, loadURLItem, quitItem;
	JMenuItem sepiaItem;

	public PhotoViewer()
	{
		super("Photo Viewer");
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
        JMenu effectMenu = new JMenu("Effects");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(fileMenu);
        effectMenu.setMnemonic(KeyEvent.VK_E);
        menuBar.add(effectMenu);
		
		loadFileItem = new JMenuItem("Load File", KeyEvent.VK_F);
		fileMenu.add(loadFileItem);
		loadFileItem.addActionListener(this);

		loadURLItem = new JMenuItem("Load URL", KeyEvent.VK_U);
		fileMenu.add(loadURLItem);
		loadURLItem.addActionListener(this);

		quitItem = new JMenuItem("Quit", KeyEvent.VK_Q);
		fileMenu.add(quitItem);
		quitItem.addActionListener(this);

        sepiaItem = new JMenuItem("Sepia", KeyEvent.VK_S);
        effectMenu.add(sepiaItem);
        sepiaItem.addActionListener(this);

		this.setJMenuBar(menuBar);
		
		this.setSize(320, 240);
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);

		pp = new PicturePage();
		this.getContentPane().add(pp);
		this.addKeyListener(pp);
		
		this.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource().equals(quitItem))
			System.exit(0);

		if(e.getSource().equals(loadFileItem))
		{
			JFileChooser fc = new JFileChooser();
			int retVal = fc.showOpenDialog(this);
			if(retVal != JFileChooser.APPROVE_OPTION)
				return;
			
			try {
				pp.setImage( ImageIO.read(fc.getSelectedFile()) );
			} catch (IOException exc) {
				// need an error dialog here
			}
			return;
		}
		
		if(e.getSource().equals(loadURLItem))
		{
			String s = (String)JOptionPane.showInputDialog(
                    this,
                    "URL please",
                    "URL Input",
                    JOptionPane.PLAIN_MESSAGE,
                    null, null, "your URL here");
			if((s!=null) && (s.length() > 0))
				try {
					pp.setImage(ImageIO.read(new URL(s)));
				} catch (IOException exc) {
					// need an error dialog again
				}
			return;
		}

        if(e.getSource().equals(sepiaItem))
        {
            pp.applyFilter( new SepiaFilter() );
            return;
        }
		
		System.out.println(e);
		System.out.println("unrecognized command \""+e.getActionCommand()+"\"");
	}
	
	public void itemStateChanged(ItemEvent e)
	{
		// copied from example. do i need this? hm
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PhotoViewer pv = new PhotoViewer();
	}

}
