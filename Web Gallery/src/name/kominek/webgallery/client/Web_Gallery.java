package name.kominek.webgallery.client;

import com.google.gwt.core.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.rpc.*;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.*;
import com.google.gwt.http.client.*;
import com.google.gwt.json.client.*;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Web_Gallery implements EntryPoint, RequestCallback {
	RootPanel rp;
	FlexTable ft = new FlexTable();
	ScrollPanel sp = new ScrollPanel();
	
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		rp = RootPanel.get("webGallery");
		rp.add(ft);
		//sp.add(ft);
		
		ft.setSize("100%", "100%");

		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET,
				"/piclist.json");

	    try {
	        Request response = builder.sendRequest(null, this);
	    } catch(RequestException e) {
	    	fatal_error("caught an exception on sendRequest");
		}
	}

	private void fatal_error(String msg)
	{
		PopupPanel pp = new PopupPanel(false, true);
		pp.add(new Label(msg));
		pp.setGlassEnabled(true);
		pp.center();		
	}
	
	public void onError(Request request, Throwable Exception)
	{
		fatal_error("onError called");
	}
	
	public void onResponseReceived(Request request, Response response)
	{
		 if (Response.SC_OK != response.getStatusCode()) {
			 fatal_error("Response was "+response.getStatusCode()+" "+response.getHeadersAsString()+" "+response.getText());
			 return;
		 }

		 JSONValue json_response;
		 try {
			 json_response = JSONParser.parse(response.getText());
		 } catch(Exception e) {
			 fatal_error("JSON failed to parse");
			 return;
		 }
		 
		 if(json_response.isArray()==null)
		 {
			 fatal_error("JSON was not an array");
			 return;
		 }		 
		 
		 int table_width = 5;
		 
		 for(int i=0; i<json_response.isArray().size(); i++)
		 {
			 JSONArray pair = json_response.isArray().get(i).isArray();

			 final String thumb_url = pair.get(0).isString().stringValue();
			 final String image_url = pair.get(1).isString().stringValue();
			 
			 Image img = new Image(thumb_url);
			 ft.setWidget(i/table_width, i%table_width, img);
			 ft.getCellFormatter().setHorizontalAlignment(i/table_width, i%table_width, HasHorizontalAlignment.ALIGN_CENTER);
			 ft.getCellFormatter().setVerticalAlignment(i/table_width, i%table_width, HasVerticalAlignment.ALIGN_MIDDLE);

			 img.addClickHandler(new ClickHandler() {
				 public void onClick(ClickEvent event) {
					 final PopupPanel pp = new PopupPanel(true, true);
					 final Image i = new Image();
					 pp.setGlassEnabled(true);
			    	 pp.add(i);
					 i.addLoadHandler(new LoadHandler() {
					      public void onLoad(LoadEvent event) {
					    	  int x = Window.getClientWidth() / 2 - i.getWidth()/2 + Window.getScrollLeft();
					    	  int y = Window.getClientHeight() / 2 - i.getHeight()/2 + Window.getScrollTop();
					    	  pp.setPopupPosition(x,y);
					      }
					 });
					 i.setUrl(image_url);
	  		    	     pp.show();
				}
			 });
		 }
	}
}
