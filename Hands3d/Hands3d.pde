/* --------------------------------------------------------------------------
 * SimpleOpenNI Hands3d Test
 * --------------------------------------------------------------------------
 * Processing Wrapper for the OpenNI/Kinect library
 * http://code.google.com/p/simple-openni
 * --------------------------------------------------------------------------
 * prog:  Max Rheiner / Interaction Design / zhdk / http://iad.zhdk.ch/
 * date:  02/27/2011 (m/d/y)
 * ----------------------------------------------------------------------------
 * This demos shows how to use the gesture/hand generator.
 * It's not the most reliable yet, a two hands example will follow
 * ----------------------------------------------------------------------------
 */
 
import SimpleOpenNI.*;
//import processing.opengl.*;

import org.sikuli.api.*;
import org.sikuli.api.robot.*;

import java.awt.event.KeyEvent;
import javax.imageio.ImageIO;


SimpleOpenNI context;
float        zoomF =0.5f;
float        rotX = radians(180);  // by default rotate the hole scene 180deg around the x-axis, 
                                   // the data from openni comes upside down
float        rotY = radians(0);
boolean      handsTrackFlag = false;
PVector      handVec = new PVector();
ArrayList    handVecList = new ArrayList();
int          handVecListSize = 24;
String       lastGesture = "";

String pathToImages = "/home/houman/workspace/sikuli/kinect/"; 

String currentOneDollarGesture = "";

void go(String direction) {
    Mouse mouse = new Mouse();
    Keyboard keyboard = new Keyboard();
    
    Target nextIconImageTarget = null;
    try {     
      nextIconImageTarget = new ColorImageTarget(ImageIO.read(new File(pathToImages + direction + ".png")));
    } catch (IOException e) {
      e.printStackTrace();
    }
    
    ScreenRegion s = new ScreenRegion();
    ScreenRegion nextIcon = s.wait(nextIconImageTarget, 5000);
    mouse.click(nextIcon.getCenter());
}

void setup()
{
  println("starting setup");
  
  size(1024,768,P3D);  // strange, get drawing error in the cameraFrustum if i use P3D, in opengl there is no problem
  //size(1024,768,OPENGL); 

  context = new SimpleOpenNI(this);
   
  // disable mirror
  context.setMirror(false);

  // enable depthMap generation 
  if(context.enableDepth() == false)
  {
     println("Can't open the depthMap, maybe the camera is not connected!"); 
     exit();
     return;
  }

  // enable hands + gesture generation
  context.enableGesture();
  context.enableHands();
  
  // add focus gestures  / here i do have some problems on the mac, i only recognize raiseHand ? Maybe cpu performance ?
  //context.addGesture("Wave");
  //context.addGesture("Click");
  context.addGesture("RaiseHand");
  
  // set how smooth the hand capturing should be
  //context.setSmoothingHands(.5);
  
  stroke(255,255,255);
  smooth();
  
  perspective(radians(45),
              float(width)/float(height),
              10.0f,150000.0f);
              
  println("finished setup");
 }

void draw()
{
  // update the cam
  context.update();

  background(0,0,0);
  
  // set the scene pos
  translate(width/2, height/2, 0);
  rotateX(rotX);
  rotateY(rotY);
  scale(zoomF);
  
  // draw the 3d point depth map
  int[]   depthMap = context.depthMap();
  int     steps   = 3;  // to speed up the drawing, draw every third point
  int     index;
  PVector realWorldPoint;
 
  translate(0,0,-1000);  // set the rotation center of the scene 1000 infront of the camera

  stroke(200); 
  for(int y=0;y < context.depthHeight();y+=steps)
  {
    for(int x=0;x < context.depthWidth();x+=steps)
    {
      index = x + y * context.depthWidth();
      if(depthMap[index] > 0)
      { 
        // draw the projected point
        realWorldPoint = context.depthMapRealWorld()[index];
        point(realWorldPoint.x,realWorldPoint.y,realWorldPoint.z); 
      }
    } 
  } 

  // draw the tracked hand
  if(handsTrackFlag)  
  {
    pushStyle();
    {
      stroke(255,0,0,200);
      noFill();
      Iterator itr = handVecList.iterator(); 
      beginShape();
        while( itr.hasNext() ) 
        { 
          PVector p = (PVector) itr.next(); 
          vertex(p.x,p.y,p.z);
        }
      endShape();   

      stroke(255,0,0);
      strokeWeight(4);
      point(handVec.x,handVec.y,handVec.z);
    }
    popStyle();   
  }
  
  // draw the kinect cam
  context.drawCamFrustum();
}


// -----------------------------------------------------------------
// hand events

void onCreateHands(int handId,PVector pos,float time)
{
  println("onCreateHands - handId: " + handId + ", pos: " + pos + ", time:" + time);

  handsTrackFlag = true;
  handVec = pos;
  
  handVecList.clear();
  handVecList.add(pos);
}

int saw_gesture = 0;

void onUpdateHands(int handId,PVector pos,float time)
{
  //println("onUpdateHandsCb - handId: " + handId + ", pos: " + pos + ", time:" + time);
  handVec = pos;

     handVecList.add(0,pos);
     if(handVecList.size() >= handVecListSize)
     { // remove the last point 
       handVecList.remove(handVecList.size()-1); 
     }
     
  saw_gesture -= 1;
  if(handVecList.size()>2 && saw_gesture<=0)
  {
    PVector ultimate = (PVector)handVecList.get(0);
    PVector penultimate = (PVector)handVecList.get(1);
    PVector derivative = PVector.sub(ultimate, penultimate);
    
    PVector right_push = new PVector(-45.0, 0.0, -100.0);
    PVector left_push = new PVector(45.0, 0.0, -100.0);
    //println("magnitude: "+derivative.mag());
    if(derivative.mag()>50.0)
    {
      float righta = PVector.angleBetween(right_push, derivative)*180.0/3.141592;
      float lefta = PVector.angleBetween(left_push, derivative)*180.0/3.141592;
      println(lefta+" "+righta);
      if(righta < 35.0)
      {
        go("Right");
        println("RIGHT PUSH!");
        saw_gesture = 2;
      } else if(lefta < 35.0) {
        go("Left");
        println("LEFT PUSH!");
        saw_gesture = 2;
      }
    }
  }

}

void onDestroyHands(int handId,float time)
{
  println("onDestroyHandsCb - handId: " + handId + ", time:" + time);
  
  handsTrackFlag = false;
  context.addGesture(lastGesture);
}

// -----------------------------------------------------------------
// gesture events

void onRecognizeGesture(String strGesture, PVector idPosition, PVector endPosition)
{
  println("onRecognizeGesture - strGesture: " + strGesture + ", idPosition: " + idPosition + ", endPosition:" + endPosition);
  
  lastGesture = strGesture;
  context.removeGesture(strGesture); 
  context.startTrackingHands(endPosition);
  
}

void onProgressGesture(String strGesture, PVector position,float progress)
{
  //println("onProgressGesture - strGesture: " + strGesture + ", position: " + position + ", progress:" + progress);
}

// -----------------------------------------------------------------
// Keyboard event
void keyPressed()
{
  switch(key)
  {
  case ' ':
    context.setMirror(!context.mirror());
    break;
  }
    
  switch(keyCode)
  {
    case LEFT:
      rotY += 0.1f;
      break;
    case RIGHT:
      rotY -= 0.1f;
      break;
    case UP:
      if(keyEvent.isShiftDown())
        zoomF += 0.01f;
      else
        rotX += 0.1f;
      break;
    case DOWN:
      if(keyEvent.isShiftDown())
      {
        zoomF -= 0.01f;
        if(zoomF < 0.01)
          zoomF = 0.01;
      }
      else
        rotX -= 0.1f;
      break;
  }
}

